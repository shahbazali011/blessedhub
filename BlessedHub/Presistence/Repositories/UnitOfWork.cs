﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
namespace BlessedHub.Presistence.Repositories
{
    public class UnitOfWork :IUnitOfWork
    {
        private readonly BlessedHubEntities _context;
      

        public IServiceRepository Services { get; private set; }
        public ICityRepository Cities { get; private set; }
        public IServiceTypeRepository ServiceTypes { get; private set; }
        public IOrganisationRepository Organisations { get; private set; }
        public IOrganisationServiceRepository OrganisationServices { get; private set; }
        public IOrganisationWorkingDaysRepository OrganisationWorkingDays { get; private set; }
        public IServiceTypeWorkingDaysDataRepository ServiceTypeWorkingDaysData { get; private set; }

        public IOrganisaitonServicesTypesRepository OrganisaitonServicesTypes { get; private set; }

        public UnitOfWork(BlessedHubEntities context)
        {
            _context = context;
            Services = new ServiceRepository(context);
            Cities = new CityRepository(context);
            ServiceTypes = new ServiceTypeRepository(context);
            Organisations = new OrganisationRepository(context);
            OrganisationServices = new OrganisationServiceRepository(context);
            OrganisationWorkingDays = new OrganisationWorkingDaysRepository(context);
            ServiceTypeWorkingDaysData = new ServiceTypeWorkingDaysDataRepository(context);
            OrganisaitonServicesTypes = new OrganisaitonServicesTypesRepository(context);
        }

        public void Complete()
        {
            _context.SaveChanges();
        }
    }
}