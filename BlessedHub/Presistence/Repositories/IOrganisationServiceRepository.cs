﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
namespace BlessedHub.Presistence.Repositories
{
    public interface IOrganisationServiceRepository
    {
        IEnumerable<OrganisationService> GetOrganisationServices();
        OrganisationService GetOrganisation(int id);
        IEnumerable<OrganisationService>  OrganisationServicesAll(int? id);
    }
}