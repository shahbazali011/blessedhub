﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
namespace BlessedHub.Presistence.Repositories
{
    public class ServiceTypeRepository : IServiceTypeRepository
    {
        private readonly BlessedHubEntities _context;
        public ServiceTypeRepository(BlessedHubEntities context)
        {
            _context = context;
        }

        public ServicesType GetServicesType(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ServicesType> GetServicesTypes()
        {
            return _context.ServicesTypes.ToList();
        }
    }
}