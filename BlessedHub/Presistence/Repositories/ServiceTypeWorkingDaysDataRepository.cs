﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
using System.Data.Entity;

namespace BlessedHub.Presistence.Repositories
{
    public class ServiceTypeWorkingDaysDataRepository : IServiceTypeWorkingDaysDataRepository
    {
        private readonly BlessedHubEntities _context;
        public ServiceTypeWorkingDaysDataRepository(BlessedHubEntities context)
        {
            _context = context;
        }
        public IEnumerable<ServiceTypeWorkingDay> GetServiceTypeWorkingDays()
        {
            var serviceTypeWorkingDays = _context.ServiceTypeWorkingDays.Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay).Include(s => s.Organisation);
            return serviceTypeWorkingDays.ToList();

        }

        public IEnumerable<ServiceTypeWorkingDay> GetServiceTypeWorkingDays(int id)
        {
            var serviceTypeWorkingDays = _context.ServiceTypeWorkingDays.Where(s => s.ServiceId == id).Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay);
            return serviceTypeWorkingDays.ToList();
        }

        public ServiceTypeWorkingDay GetWorkingDay(int id)
        {
            return _context.ServiceTypeWorkingDays.Find(id);
        }

        public IEnumerable<ServiceTypeWorkingDay> PartialAllServicesTypesDetails(int? id)
        {
            var serviceTypeWorkingDays = _context.ServiceTypeWorkingDays.Where(s => s.ServiceId == id).Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay);
            return serviceTypeWorkingDays.ToList();

        }

        public IEnumerable<ServiceTypeWorkingDay> PartialMapOrganisation(int? id)
        {
            var serviceTypeWorkingDays = _context.ServiceTypeWorkingDays.Where(s => s.ServiceId == id).Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay);
            return serviceTypeWorkingDays.ToList();
        }

        public IEnumerable<ServiceTypeWorkingDay> PartialServiceTypeTimeTable(int? ServiceTypeId, int? ServiceId, int? OrganisationId)
        {
            var serviceTypeWorkingDays = _context.ServiceTypeWorkingDays
             .Where(st => st.ServiceTypeId == ServiceTypeId && st.ServiceId == 1 && st.OrganisationId == 1)
             .Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay).Include(s => s.Organisation);
            return serviceTypeWorkingDays.ToList();
        }
    }
}