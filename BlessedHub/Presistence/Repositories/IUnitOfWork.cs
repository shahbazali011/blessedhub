﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlessedHub.Models;

namespace BlessedHub.Presistence.Repositories
{
    public interface IUnitOfWork
    {
        IServiceRepository Services { get; }
        ICityRepository Cities { get; }
        IOrganisationRepository Organisations { get; }
        IServiceTypeRepository ServiceTypes { get; }
        IOrganisationServiceRepository OrganisationServices { get; }
        IOrganisationWorkingDaysRepository OrganisationWorkingDays { get; }
        IServiceTypeWorkingDaysDataRepository ServiceTypeWorkingDaysData { get; }
        IOrganisaitonServicesTypesRepository OrganisaitonServicesTypes { get; }







        void Complete();
    }
}
