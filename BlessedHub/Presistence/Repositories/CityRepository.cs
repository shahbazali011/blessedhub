﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
namespace BlessedHub.Presistence.Repositories
{
    public class CityRepository : ICityRepository
    {
        private readonly BlessedHubEntities _context;
        public CityRepository(BlessedHubEntities context)
        {
            _context = context;
        }

        public IEnumerable<City> GetCities()
        {
            return _context.Cities.ToList();
        }

        public City GetCity(int id)
        {
            return _context.Cities.SingleOrDefault(c => c.CityId == id);
        }

        
    }
}