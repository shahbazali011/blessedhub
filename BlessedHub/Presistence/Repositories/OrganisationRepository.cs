﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
using System.Data.Entity;
namespace BlessedHub.Presistence.Repositories
{
    public class OrganisationRepository : IOrganisationRepository
    {
        private readonly BlessedHubEntities _context;
        public OrganisationRepository(BlessedHubEntities context)
        {
            _context = context;
        }

        public Organisation GetOrganisation(int id)
        {
            return _context.Organisations.Include(c =>c.City1).SingleOrDefault(o => o.OrganisationId == id);
        }

        public IEnumerable<Organisation> GetOrganisations()
        {
            var organisations = _context.Organisations.Include(o => o.City1);

            return organisations.ToList();
        }

        public IEnumerable<Organisation> GetPostCodeList(string postcode)

        {
            //postcode = postcode.Substring(0, 3);
            var organisations = _context.Organisations.Where(pc =>pc.PostCode==postcode);

            return organisations.ToList();
        }
    }
}