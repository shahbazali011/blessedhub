﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
using System.Data.Entity;

namespace BlessedHub.Presistence.Repositories
{
    public class OrganisaitonServicesTypesRepository : IOrganisaitonServicesTypesRepository
    {
        private readonly BlessedHubEntities _context;

        public OrganisaitonServicesTypesRepository(BlessedHubEntities context)
        {
            _context = context;
        }
        public OrganisaitonServicesType OrganisaitonServicesType(int id)
        {


            return _context.OrganisaitonServicesTypes.Find(id); 
        }

        public IEnumerable<OrganisaitonServicesType> OrganisaitonServicesTypes()
        {
            var organisaitonServicesTypes = _context.OrganisaitonServicesTypes.Include(o => o.Organisation).Include(o => o.ServicesType);
            return organisaitonServicesTypes.ToList();
        }

        public IEnumerable<OrganisaitonServicesType> OrganisaitonServicesTypesAll(int? OrganisationId, int? ServiceId)
        {
            var organisaitonServicesTypes = _context.OrganisaitonServicesTypes
              .Where(or => or.Organisation.OrganisationId == OrganisationId && or.ServicesType.ServiceId == ServiceId)
              .Include(o => o.Organisation).Include(o => o.ServicesType).Include(o => o.Service);
            return organisaitonServicesTypes.ToList();
        }

        public IEnumerable<OrganisaitonServicesType> PartialServicesTypesCard(int? OrganisationId, int? OrganisationServicesId)
        {
            var organisaitonServicesTypes = _context.OrganisaitonServicesTypes
               .Where(or => or.Organisation.OrganisationId == OrganisationId && or.ServicesType.ServiceId == OrganisationServicesId)
               .Include(o => o.Organisation).Include(o => o.ServicesType).Include(o => o.Service);
            return organisaitonServicesTypes.ToList();
        }
    }
}