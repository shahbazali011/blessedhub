﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlessedHub.Models;
namespace BlessedHub.Presistence.Repositories
{
    public interface IServiceTypeRepository
    {
        IEnumerable<ServicesType> GetServicesTypes();
        ServicesType GetServicesType(int id);

    }
}
