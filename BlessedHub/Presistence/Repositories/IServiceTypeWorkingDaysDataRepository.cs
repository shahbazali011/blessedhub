﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;

namespace BlessedHub.Presistence.Repositories
{
   public interface IServiceTypeWorkingDaysDataRepository
    {
        IEnumerable<ServiceTypeWorkingDay> GetServiceTypeWorkingDays();
        ServiceTypeWorkingDay GetWorkingDay(int id);
        IEnumerable<ServiceTypeWorkingDay> GetServiceTypeWorkingDays(int id);
        IEnumerable<ServiceTypeWorkingDay> PartialAllServicesTypesDetails(int? id);
        IEnumerable<ServiceTypeWorkingDay> PartialMapOrganisation(int? id);
        IEnumerable<ServiceTypeWorkingDay>  PartialServiceTypeTimeTable(int? ServiceTypeId, int? ServiceId, int? OrganisationId);
    }
}
