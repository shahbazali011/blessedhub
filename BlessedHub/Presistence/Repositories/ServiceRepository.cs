﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
namespace BlessedHub.Presistence.Repositories
{
    public class ServiceRepository : IServiceRepository
    {
        private readonly BlessedHubEntities _context;
        public ServiceRepository(BlessedHubEntities context)
        {
            _context = context;
        }

        public Service GetService(int id)
        {
            return _context.Services.SingleOrDefault(s => s.ServiceId == id);
        }

        public IEnumerable<Service> GetServices()
        {
            return _context.Services.ToList();
        }
    }
}