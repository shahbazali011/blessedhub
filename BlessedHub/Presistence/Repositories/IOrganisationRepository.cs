﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlessedHub.Models;
namespace BlessedHub.Presistence.Repositories
{
    public interface IOrganisationRepository
    {
        IEnumerable<Organisation> GetOrganisations();
        Organisation GetOrganisation(int id);
        IEnumerable<Organisation> GetPostCodeList(string postcode);

    }
}
