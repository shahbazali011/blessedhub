﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
namespace BlessedHub.Presistence.Repositories
{
    public interface IOrganisationWorkingDaysRepository
    {
        IEnumerable<OrganisationWorkingDay> GetOrganisationWorkingDays();
        OrganisationWorkingDay GetWorkingDay(int id);
        IEnumerable<OrganisationWorkingDay> OranisationTimeTable(int? id);
    
    }
}