﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Presistence.Repositories
{
    public class OrganisationWorkingDaysRepository :IOrganisationWorkingDaysRepository
    {
        private readonly BlessedHubEntities _context;
        public OrganisationWorkingDaysRepository(BlessedHubEntities context)
        {
            _context = context;
        }

        public IEnumerable<OrganisationWorkingDay> GetOrganisationWorkingDays()
        {
            var organisationWorkingDays = _context.OrganisationWorkingDays.Include(o => o.Organisation).Include(o => o.WeekDay);
            return (organisationWorkingDays.ToList());
        }

        public OrganisationWorkingDay GetWorkingDay(int id)
        {
            return _context.OrganisationWorkingDays.Find(id);
        }

        public IEnumerable<OrganisationWorkingDay> OranisationTimeTable(int? id)
        {
            var organisationWorkingDays = _context.OrganisationWorkingDays.Where(o => o.OrganisationId == id).Include(o => o.Organisation).Include(o => o.WeekDay);
            return (organisationWorkingDays.ToList());
        }


    }
}