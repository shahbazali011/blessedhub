﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlessedHub.Models;

namespace BlessedHub.Presistence.Repositories
{
   public interface IOrganisaitonServicesTypesRepository
    {
        IEnumerable<OrganisaitonServicesType> OrganisaitonServicesTypes();
        OrganisaitonServicesType OrganisaitonServicesType(int id);
        IEnumerable<OrganisaitonServicesType> OrganisaitonServicesTypesAll(int? OrganisationId, int? OrganisationServicesId);
        IEnumerable<OrganisaitonServicesType> PartialServicesTypesCard(int? OrganisationId, int? OrganisationServicesId);
    }
}
