﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BlessedHub.Models;
using System.Data.Entity;
namespace BlessedHub.Presistence.Repositories
{
    public class OrganisationServiceRepository : IOrganisationServiceRepository
    {
        private readonly BlessedHubEntities _context;
        public OrganisationServiceRepository(BlessedHubEntities context)
        {
            _context = context;
        }

        public IEnumerable<OrganisationService> OrganisationServicesAll(int? id)
        {
            var organisationServices = _context.OrganisationServices.Where(s => s.Service.ServiceId == id).Include(o => o.Organisation).Include(o => o.Service);

            return organisationServices.ToList();
        }

        public OrganisationService GetOrganisation(int id)
        {

            return _context.OrganisationServices.Find(id);
        }

        public IEnumerable<OrganisationService> GetOrganisationServices()
        {
            var organisationServices = _context.OrganisationServices.Include(o => o.Organisation).Include(o => o.Service);
            return organisationServices.ToList();
            //return _context.OrganisationServices.Include(o => o.Organisation).Include(st => st.ServicesType).Include(os => os.organisationService).ToList();
        }
    }
}