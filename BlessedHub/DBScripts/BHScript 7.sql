USE [master]
GO
/****** Object:  Database [BlessedHub]    Script Date: 24/06/2019 14:54:28 ******/
CREATE DATABASE [BlessedHub]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BlessedHub', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SHAHBAZ\MSSQL\DATA\BlessedHub.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BlessedHub_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SHAHBAZ\MSSQL\DATA\BlessedHub_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BlessedHub] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BlessedHub].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BlessedHub] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BlessedHub] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BlessedHub] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BlessedHub] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BlessedHub] SET ARITHABORT OFF 
GO
ALTER DATABASE [BlessedHub] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BlessedHub] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BlessedHub] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BlessedHub] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BlessedHub] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BlessedHub] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BlessedHub] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BlessedHub] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BlessedHub] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BlessedHub] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BlessedHub] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BlessedHub] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BlessedHub] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BlessedHub] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BlessedHub] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BlessedHub] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BlessedHub] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BlessedHub] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BlessedHub] SET  MULTI_USER 
GO
ALTER DATABASE [BlessedHub] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BlessedHub] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BlessedHub] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BlessedHub] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BlessedHub] SET DELAYED_DURABILITY = DISABLED 
GO
USE [BlessedHub]
GO
/****** Object:  Table [dbo].[Cities]    Script Date: 24/06/2019 14:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cities](
	[CityId] [int] IDENTITY(1,1) NOT NULL,
	[CityName] [varchar](500) NULL,
 CONSTRAINT [PK_Cities] PRIMARY KEY CLUSTERED 
(
	[CityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrganisaitonServicesTypes]    Script Date: 24/06/2019 14:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrganisaitonServicesTypes](
	[OrganisaitonServicesTypesId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceTypeId] [int] NOT NULL,
	[OrganisationId] [int] NOT NULL,
	[SuitableFor] [varchar](50) NULL,
	[Description] [varchar](max) NULL,
 CONSTRAINT [PK_OrganisaitonServicesTypes] PRIMARY KEY CLUSTERED 
(
	[OrganisaitonServicesTypesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Organisation]    Script Date: 24/06/2019 14:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Organisation](
	[OrganisationId] [int] IDENTITY(1,1) NOT NULL,
	[CityId] [int] NOT NULL,
	[Name] [varchar](500) NULL,
	[Type] [varchar](500) NULL,
	[Title] [varchar](500) NULL,
	[SubTitle] [varchar](500) NULL,
	[Description] [varchar](max) NULL,
	[Address1] [varchar](500) NULL,
	[Address2] [varchar](500) NULL,
	[Address3] [varchar](500) NULL,
	[PostCode] [varchar](500) NULL,
	[City] [varchar](500) NULL,
	[Telephone] [varchar](500) NULL,
	[Email] [varchar](500) NULL,
	[Website] [varchar](500) NULL,
	[Twitter] [varchar](500) NULL,
	[Facebook] [varchar](500) NULL,
 CONSTRAINT [PK_Organisation] PRIMARY KEY CLUSTERED 
(
	[OrganisationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrganisationServices]    Script Date: 24/06/2019 14:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrganisationServices](
	[OrganisationServicesId] [int] IDENTITY(1,1) NOT NULL,
	[OrganisationId] [int] NOT NULL,
	[Description] [varchar](max) NULL,
	[ServiceId] [int] NOT NULL CONSTRAINT [DF_OrganisationServices_ServiceId]  DEFAULT ((1)),
 CONSTRAINT [PK_OrganisationServices] PRIMARY KEY CLUSTERED 
(
	[OrganisationServicesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrganisationServicesTEST]    Script Date: 24/06/2019 14:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrganisationServicesTEST](
	[OrganisationServicesId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceId] [int] NOT NULL,
	[OrganisationId] [int] NOT NULL,
 CONSTRAINT [PK_OrganisationServicesTEST] PRIMARY KEY CLUSTERED 
(
	[OrganisationServicesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Services]    Script Date: 24/06/2019 14:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Services](
	[ServiceId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceName] [varchar](500) NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED 
(
	[ServiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ServicesTimings]    Script Date: 24/06/2019 14:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServicesTimings](
	[ServiceTimingId] [int] NOT NULL,
	[Monday] [bit] NULL,
	[Tuesday] [bit] NULL,
	[Wednesday] [bit] NULL,
	[Thursday] [bit] NULL,
	[Friday] [bit] NULL,
	[Saturday] [bit] NULL,
	[Sunday] [bit] NULL,
	[OrganisaitonId] [int] NOT NULL,
	[ServiceTypeId] [int] NOT NULL,
 CONSTRAINT [PK_ServicesTimings] PRIMARY KEY CLUSTERED 
(
	[ServiceTimingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServicesTypes]    Script Date: 24/06/2019 14:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ServicesTypes](
	[ServiceTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceType] [varchar](500) NULL,
	[ServiceId] [int] NOT NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_ServicesTypes] PRIMARY KEY CLUSTERED 
(
	[ServiceTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TimeTableServices]    Script Date: 24/06/2019 14:54:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TimeTableServices](
	[TimeTableServicesId] [int] IDENTITY(1,1) NOT NULL,
	[StartTime] [varchar](50) NULL,
	[EndTime] [nchar](10) NULL,
	[ServiceTimingId] [int] NOT NULL,
 CONSTRAINT [PK_TimeTable] PRIMARY KEY CLUSTERED 
(
	[TimeTableServicesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes]  WITH CHECK ADD  CONSTRAINT [FK_OrganisaitonServicesTypes_Organisation] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes] CHECK CONSTRAINT [FK_OrganisaitonServicesTypes_Organisation]
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes]  WITH CHECK ADD  CONSTRAINT [FK_OrganisaitonServicesTypes_ServicesTypes] FOREIGN KEY([ServiceTypeId])
REFERENCES [dbo].[ServicesTypes] ([ServiceTypeId])
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes] CHECK CONSTRAINT [FK_OrganisaitonServicesTypes_ServicesTypes]
GO
ALTER TABLE [dbo].[Organisation]  WITH CHECK ADD  CONSTRAINT [FK_Organisation_Organisation] FOREIGN KEY([CityId])
REFERENCES [dbo].[Cities] ([CityId])
GO
ALTER TABLE [dbo].[Organisation] CHECK CONSTRAINT [FK_Organisation_Organisation]
GO
ALTER TABLE [dbo].[OrganisationServices]  WITH CHECK ADD  CONSTRAINT [FK_OrganisationServices_Organisation] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[OrganisationServices] CHECK CONSTRAINT [FK_OrganisationServices_Organisation]
GO
ALTER TABLE [dbo].[OrganisationServices]  WITH CHECK ADD  CONSTRAINT [FK_OrganisationServices_Services] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Services] ([ServiceId])
GO
ALTER TABLE [dbo].[OrganisationServices] CHECK CONSTRAINT [FK_OrganisationServices_Services]
GO
ALTER TABLE [dbo].[ServicesTimings]  WITH CHECK ADD  CONSTRAINT [FK_ServicesTimings_Organisation] FOREIGN KEY([OrganisaitonId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[ServicesTimings] CHECK CONSTRAINT [FK_ServicesTimings_Organisation]
GO
ALTER TABLE [dbo].[ServicesTimings]  WITH CHECK ADD  CONSTRAINT [FK_ServicesTimings_ServicesTypes] FOREIGN KEY([ServiceTypeId])
REFERENCES [dbo].[ServicesTypes] ([ServiceTypeId])
GO
ALTER TABLE [dbo].[ServicesTimings] CHECK CONSTRAINT [FK_ServicesTimings_ServicesTypes]
GO
ALTER TABLE [dbo].[ServicesTypes]  WITH CHECK ADD  CONSTRAINT [FK_ServicesTypes_Services] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Services] ([ServiceId])
GO
ALTER TABLE [dbo].[ServicesTypes] CHECK CONSTRAINT [FK_ServicesTypes_Services]
GO
ALTER TABLE [dbo].[TimeTableServices]  WITH CHECK ADD  CONSTRAINT [FK_TimeTableServices_ServicesTimings] FOREIGN KEY([ServiceTimingId])
REFERENCES [dbo].[ServicesTimings] ([ServiceTimingId])
GO
ALTER TABLE [dbo].[TimeTableServices] CHECK CONSTRAINT [FK_TimeTableServices_ServicesTimings]
GO
USE [master]
GO
ALTER DATABASE [BlessedHub] SET  READ_WRITE 
GO
