USE [master]
GO
/****** Object:  Database [BlessedHub]    Script Date: 03/07/2019 15:05:04 ******/
CREATE DATABASE [BlessedHub]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BlessedHub', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SHAHBAZ\MSSQL\DATA\BlessedHub.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BlessedHub_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SHAHBAZ\MSSQL\DATA\BlessedHub_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BlessedHub] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BlessedHub].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BlessedHub] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BlessedHub] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BlessedHub] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BlessedHub] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BlessedHub] SET ARITHABORT OFF 
GO
ALTER DATABASE [BlessedHub] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BlessedHub] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BlessedHub] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BlessedHub] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BlessedHub] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BlessedHub] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BlessedHub] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BlessedHub] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BlessedHub] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BlessedHub] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BlessedHub] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BlessedHub] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BlessedHub] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BlessedHub] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BlessedHub] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BlessedHub] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BlessedHub] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BlessedHub] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BlessedHub] SET  MULTI_USER 
GO
ALTER DATABASE [BlessedHub] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BlessedHub] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BlessedHub] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BlessedHub] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BlessedHub] SET DELAYED_DURABILITY = DISABLED 
GO
USE [BlessedHub]
GO
/****** Object:  User [IIS APPPOOL\BlessedHubLive]    Script Date: 03/07/2019 15:05:04 ******/
CREATE USER [IIS APPPOOL\BlessedHubLive] FOR LOGIN [IIS APPPOOL\BlessedHubLive] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [IIS APPPOOL\BlessedHubLive]
GO
/****** Object:  Table [dbo].[Cities]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cities](
	[CityId] [int] IDENTITY(1,1) NOT NULL,
	[CityName] [varchar](500) NULL,
 CONSTRAINT [PK_Cities] PRIMARY KEY CLUSTERED 
(
	[CityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DonationMoneyTypesGiveHelp]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DonationMoneyTypesGiveHelp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DonationMoneyTypes] [varchar](500) NULL,
	[Description] [varchar](max) NULL,
 CONSTRAINT [PK_DonationMoneyTypesGiveHelp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ItemTypesGiveHelp]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ItemTypesGiveHelp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemsTypes] [varchar](max) NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_ItemTypesGiveHelp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MapLocationOrganisation]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MapLocationOrganisation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrganisationId] [int] NOT NULL,
	[PostCode] [varchar](50) NULL,
	[Latitude] [decimal](18, 6) NULL,
	[Longitude] [decimal](18, 6) NULL,
 CONSTRAINT [PK_MapLocationOrganisation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrganisaitonNeedHelp]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrganisaitonNeedHelp](
	[Id] [int] NOT NULL,
	[ItemTypeId] [int] NOT NULL,
	[VolunteerTypeId] [int] NOT NULL,
	[DonationTypeId] [int] NOT NULL,
	[OrganizationId] [int] NOT NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_OrganisaitonNeedHelp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrganisaitonServicesTypes]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrganisaitonServicesTypes](
	[OrganisaitonServicesTypesId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceTypeId] [int] NOT NULL,
	[OrganisationId] [int] NOT NULL,
	[SuitableFor] [varchar](50) NULL,
	[Description] [varchar](max) NULL,
 CONSTRAINT [PK_OrganisaitonServicesTypes] PRIMARY KEY CLUSTERED 
(
	[OrganisaitonServicesTypesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Organisation]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Organisation](
	[OrganisationId] [int] IDENTITY(1,1) NOT NULL,
	[CityId] [int] NOT NULL,
	[Name] [varchar](500) NULL,
	[Type] [varchar](500) NULL,
	[Title] [varchar](500) NULL,
	[SubTitle] [varchar](500) NULL,
	[Description] [varchar](max) NULL,
	[Address1] [varchar](500) NULL,
	[Address2] [varchar](500) NULL,
	[Address3] [varchar](500) NULL,
	[PostCode] [varchar](500) NULL,
	[Latitude] [decimal](18, 6) NULL,
	[Longitude] [decimal](18, 6) NULL,
	[City] [varchar](500) NULL,
	[Telephone] [varchar](500) NULL,
	[Email] [varchar](500) NULL,
	[Website] [varchar](500) NULL,
	[Twitter] [varchar](500) NULL,
	[Facebook] [varchar](500) NULL,
 CONSTRAINT [PK_Organisation] PRIMARY KEY CLUSTERED 
(
	[OrganisationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrganisationServices]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrganisationServices](
	[OrganisationServicesId] [int] IDENTITY(1,1) NOT NULL,
	[OrganisationId] [int] NOT NULL,
	[Description] [varchar](max) NULL,
	[ServiceId] [int] NOT NULL CONSTRAINT [DF_OrganisationServices_ServiceId]  DEFAULT ((1)),
 CONSTRAINT [PK_OrganisationServices] PRIMARY KEY CLUSTERED 
(
	[OrganisationServicesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrganisationWorkingDays]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OrganisationWorkingDays](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DayId] [int] NULL,
	[OrganisationId] [int] NULL,
	[WorkingDay] [bit] NULL,
	[StartTime] [varchar](50) NULL,
	[EndTime] [varchar](50) NULL,
 CONSTRAINT [PK_OrganisationWorkingDays] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Roles] [varchar](50) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Services]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Services](
	[ServiceId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceName] [varchar](500) NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED 
(
	[ServiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ServicesTypes]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ServicesTypes](
	[ServiceTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ServiceType] [varchar](500) NULL,
	[ServiceId] [int] NOT NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_ServicesTypes] PRIMARY KEY CLUSTERED 
(
	[ServiceTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ServiceTypeWorkingDays]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ServiceTypeWorkingDays](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DayId] [int] NULL,
	[ServiceId] [int] NOT NULL,
	[ServiceTypeId] [int] NOT NULL,
	[WorkingDay] [bit] NULL,
	[StartTime] [varchar](50) NULL,
	[EndTime] [varchar](50) NULL,
	[OrganisationId] [int] NOT NULL CONSTRAINT [DF_ServiceTypeWorkingDays_OrganisationId]  DEFAULT ((1)),
 CONSTRAINT [PK_ServiceTypeWorkingDays] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[isActive] [bit] NULL,
	[Email] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[PhoneNumber] [varchar](50) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VolunteerTypesGiveHelp]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VolunteerTypesGiveHelp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VounteerType] [varchar](500) NULL,
	[Description] [varchar](500) NULL,
 CONSTRAINT [PK_VolunteerTypesGiveHelp] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WeekDays]    Script Date: 03/07/2019 15:05:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WeekDays](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Days] [varchar](50) NULL,
 CONSTRAINT [PK_WeekDays] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[OrganisaitonNeedHelp]  WITH CHECK ADD  CONSTRAINT [FK_OrganisaitonNeedHelp_DonationMoneyTypesGiveHelp] FOREIGN KEY([DonationTypeId])
REFERENCES [dbo].[DonationMoneyTypesGiveHelp] ([Id])
GO
ALTER TABLE [dbo].[OrganisaitonNeedHelp] CHECK CONSTRAINT [FK_OrganisaitonNeedHelp_DonationMoneyTypesGiveHelp]
GO
ALTER TABLE [dbo].[OrganisaitonNeedHelp]  WITH CHECK ADD  CONSTRAINT [FK_OrganisaitonNeedHelp_ItemTypesGiveHelp] FOREIGN KEY([ItemTypeId])
REFERENCES [dbo].[ItemTypesGiveHelp] ([Id])
GO
ALTER TABLE [dbo].[OrganisaitonNeedHelp] CHECK CONSTRAINT [FK_OrganisaitonNeedHelp_ItemTypesGiveHelp]
GO
ALTER TABLE [dbo].[OrganisaitonNeedHelp]  WITH CHECK ADD  CONSTRAINT [FK_OrganisaitonNeedHelp_Organisation] FOREIGN KEY([OrganizationId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[OrganisaitonNeedHelp] CHECK CONSTRAINT [FK_OrganisaitonNeedHelp_Organisation]
GO
ALTER TABLE [dbo].[OrganisaitonNeedHelp]  WITH CHECK ADD  CONSTRAINT [FK_OrganisaitonNeedHelp_VolunteerTypesGiveHelp] FOREIGN KEY([VolunteerTypeId])
REFERENCES [dbo].[VolunteerTypesGiveHelp] ([Id])
GO
ALTER TABLE [dbo].[OrganisaitonNeedHelp] CHECK CONSTRAINT [FK_OrganisaitonNeedHelp_VolunteerTypesGiveHelp]
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes]  WITH CHECK ADD  CONSTRAINT [FK_OrganisaitonServicesTypes_Organisation] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes] CHECK CONSTRAINT [FK_OrganisaitonServicesTypes_Organisation]
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes]  WITH CHECK ADD  CONSTRAINT [FK_OrganisaitonServicesTypes_ServicesTypes] FOREIGN KEY([ServiceTypeId])
REFERENCES [dbo].[ServicesTypes] ([ServiceTypeId])
GO
ALTER TABLE [dbo].[OrganisaitonServicesTypes] CHECK CONSTRAINT [FK_OrganisaitonServicesTypes_ServicesTypes]
GO
ALTER TABLE [dbo].[Organisation]  WITH CHECK ADD  CONSTRAINT [FK_Organisation_Organisation] FOREIGN KEY([CityId])
REFERENCES [dbo].[Cities] ([CityId])
GO
ALTER TABLE [dbo].[Organisation] CHECK CONSTRAINT [FK_Organisation_Organisation]
GO
ALTER TABLE [dbo].[OrganisationServices]  WITH CHECK ADD  CONSTRAINT [FK_OrganisationServices_Organisation] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[OrganisationServices] CHECK CONSTRAINT [FK_OrganisationServices_Organisation]
GO
ALTER TABLE [dbo].[OrganisationServices]  WITH CHECK ADD  CONSTRAINT [FK_OrganisationServices_Services] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Services] ([ServiceId])
GO
ALTER TABLE [dbo].[OrganisationServices] CHECK CONSTRAINT [FK_OrganisationServices_Services]
GO
ALTER TABLE [dbo].[OrganisationWorkingDays]  WITH CHECK ADD  CONSTRAINT [FK_OrganisationWorkingDays_Organisation] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[OrganisationWorkingDays] CHECK CONSTRAINT [FK_OrganisationWorkingDays_Organisation]
GO
ALTER TABLE [dbo].[OrganisationWorkingDays]  WITH CHECK ADD  CONSTRAINT [FK_OrganisationWorkingDays_WeekDays] FOREIGN KEY([DayId])
REFERENCES [dbo].[WeekDays] ([Id])
GO
ALTER TABLE [dbo].[OrganisationWorkingDays] CHECK CONSTRAINT [FK_OrganisationWorkingDays_WeekDays]
GO
ALTER TABLE [dbo].[ServicesTypes]  WITH CHECK ADD  CONSTRAINT [FK_ServicesTypes_Services] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Services] ([ServiceId])
GO
ALTER TABLE [dbo].[ServicesTypes] CHECK CONSTRAINT [FK_ServicesTypes_Services]
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTypeWorkingDays_Organisation] FOREIGN KEY([OrganisationId])
REFERENCES [dbo].[Organisation] ([OrganisationId])
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays] CHECK CONSTRAINT [FK_ServiceTypeWorkingDays_Organisation]
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTypeWorkingDays_Services] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[Services] ([ServiceId])
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays] CHECK CONSTRAINT [FK_ServiceTypeWorkingDays_Services]
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTypeWorkingDays_ServicesTypes] FOREIGN KEY([ServiceTypeId])
REFERENCES [dbo].[ServicesTypes] ([ServiceTypeId])
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays] CHECK CONSTRAINT [FK_ServiceTypeWorkingDays_ServicesTypes]
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays]  WITH CHECK ADD  CONSTRAINT [FK_ServiceTypeWorkingDays_WeekDays] FOREIGN KEY([DayId])
REFERENCES [dbo].[WeekDays] ([Id])
GO
ALTER TABLE [dbo].[ServiceTypeWorkingDays] CHECK CONSTRAINT [FK_ServiceTypeWorkingDays_WeekDays]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Roles]
GO
ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Users]
GO
USE [master]
GO
ALTER DATABASE [BlessedHub] SET  READ_WRITE 
GO
