﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.Presistence.Repositories;

namespace BlessedHub.Controllers
{
    public class OrganisaitonServicesTypesDataController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;
        public OrganisaitonServicesTypesDataController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

        }

        // GET: OrganisaitonServicesTypesData
        public ActionResult Index()
        {
            var organisaitonServicesTypes = db.OrganisaitonServicesTypes.Include(o => o.Organisation).Include(o => o.ServicesType).Include(o => o.Service);
            return View(organisaitonServicesTypes.ToList());
        }

        public ActionResult OrganisaitonServicesTypesAll(int? OrganisationId, int? ServiceId)
        {

            return PartialView("_PartialOrganisationServicesTypes", _unitOfWork.OrganisaitonServicesTypes.OrganisaitonServicesTypesAll(OrganisationId, ServiceId));
        }


        public ActionResult PartialServicesTypesCard(int? OrganisationId, int? OrganisationServicesId)
        {

            return PartialView("_PartialServicesTypesCard", _unitOfWork.OrganisaitonServicesTypes.PartialServicesTypesCard(OrganisationId, OrganisationServicesId));
        }
        // GET: OrganisaitonServicesTypesData/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganisaitonServicesType organisaitonServicesType = db.OrganisaitonServicesTypes.Find(id);
            if (organisaitonServicesType == null)
            {
                return HttpNotFound();
            }
            return View(organisaitonServicesType);
        }

        // GET: OrganisaitonServicesTypesData/Create
        public ActionResult Create()
        {
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name");
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType");
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName");
            return View();
        }

        // POST: OrganisaitonServicesTypesData/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrganisaitonServicesTypesId,ServiceTypeId,OrganisationId,SuitableFor,Description,ServiceId")] OrganisaitonServicesType organisaitonServicesType)
        {
            if (ModelState.IsValid)
            {
                db.OrganisaitonServicesTypes.Add(organisaitonServicesType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", organisaitonServicesType.OrganisationId);
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType", organisaitonServicesType.ServiceTypeId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName", organisaitonServicesType.ServiceId);
            return View(organisaitonServicesType);
        }

        // GET: OrganisaitonServicesTypesData/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganisaitonServicesType organisaitonServicesType = db.OrganisaitonServicesTypes.Find(id);
            if (organisaitonServicesType == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", organisaitonServicesType.OrganisationId);
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType", organisaitonServicesType.ServiceTypeId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName", organisaitonServicesType.ServiceId);
            return View(organisaitonServicesType);
        }

        // POST: OrganisaitonServicesTypesData/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrganisaitonServicesTypesId,ServiceTypeId,OrganisationId,SuitableFor,Description,ServiceId")] OrganisaitonServicesType organisaitonServicesType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organisaitonServicesType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", organisaitonServicesType.OrganisationId);
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType", organisaitonServicesType.ServiceTypeId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName", organisaitonServicesType.ServiceId);
            return View(organisaitonServicesType);
        }

        // GET: OrganisaitonServicesTypesData/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganisaitonServicesType organisaitonServicesType = db.OrganisaitonServicesTypes.Find(id);
            if (organisaitonServicesType == null)
            {
                return HttpNotFound();
            }
            return View(organisaitonServicesType);
        }

        // POST: OrganisaitonServicesTypesData/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrganisaitonServicesType organisaitonServicesType = db.OrganisaitonServicesTypes.Find(id);
            db.OrganisaitonServicesTypes.Remove(organisaitonServicesType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
