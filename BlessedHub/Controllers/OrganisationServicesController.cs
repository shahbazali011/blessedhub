﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.CustomAuthentication;
using BlessedHub.Models;
using BlessedHub.Presistence.Repositories;

namespace BlessedHub.Controllers
{
    public class OrganisationServicesController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();

        private readonly IUnitOfWork _unitOfWork;
        public OrganisationServicesController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

        }

        // GET: OrganisationServices
        public ActionResult Index()
        {
            var organisationServices = db.OrganisationServices.Include(o => o.Organisation).Include(o => o.Service);
            return View(organisationServices.ToList());
        }
      
        public ActionResult OrganisationServicesAll(int? id)
        {
            //var organisationServices = db.OrganisationServices.Where(s =>s.Service.ServiceId==id).Include(o => o.Organisation).Include(o => o.Service);
            //return PartialView("_PartialOrganisationServicecs", organisationServices.ToList());
            return PartialView("_PartialOrganisationServicecs", _unitOfWork.OrganisationServices.OrganisationServicesAll(id));
        }

        // GET: OrganisationServices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganisationService organisationService = db.OrganisationServices.Find(id);
            if (organisationService == null)
            {
                return HttpNotFound();
            }
            return View(organisationService);
        }

        // GET: OrganisationServices/Create
        public ActionResult Create()
        {
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name");
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName");
            return View();
        }

        // POST: OrganisationServices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OrganisationServicesId,OrganisationId,Description,ServiceId")] OrganisationService organisationService)
        {
            if (ModelState.IsValid)
            {
                db.OrganisationServices.Add(organisationService);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", organisationService.OrganisationId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName", organisationService.ServiceId);
            return View(organisationService);
        }

        // GET: OrganisationServices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganisationService organisationService = db.OrganisationServices.Find(id);
            if (organisationService == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", organisationService.OrganisationId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName", organisationService.ServiceId);
            return View(organisationService);
        }

        // POST: OrganisationServices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OrganisationServicesId,OrganisationId,Description,ServiceId")] OrganisationService organisationService)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organisationService).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", organisationService.OrganisationId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName", organisationService.ServiceId);
            return View(organisationService);
        }

        // GET: OrganisationServices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganisationService organisationService = db.OrganisationServices.Find(id);
            if (organisationService == null)
            {
                return HttpNotFound();
            }
            return View(organisationService);
        }

        // POST: OrganisationServices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrganisationService organisationService = db.OrganisationServices.Find(id);
            db.OrganisationServices.Remove(organisationService);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
