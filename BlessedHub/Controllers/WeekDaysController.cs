﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class WeekDaysController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();

        // GET: WeekDays
        public ActionResult Index()
        {
            return View(db.WeekDays.ToList());
        }


        public ActionResult PartialWeekDasys(int? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //WeekDay weekDay = db.WeekDays.Find(id);
            //if (weekDay == null)
            //{
            //    return HttpNotFound();
            //}
            return PartialView("_PartialWeekDays", db.WeekDays.ToList());
        }


        // GET: WeekDays/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WeekDay weekDay = db.WeekDays.Find(id);
            if (weekDay == null)
            {
                return HttpNotFound();
            }
            return View(weekDay);
        }

        // GET: WeekDays/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: WeekDays/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Days")] WeekDay weekDay)
        {
            if (ModelState.IsValid)
            {
                db.WeekDays.Add(weekDay);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(weekDay);
        }

        // GET: WeekDays/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WeekDay weekDay = db.WeekDays.Find(id);
            if (weekDay == null)
            {
                return HttpNotFound();
            }
            return View(weekDay);
        }

        // POST: WeekDays/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Days")] WeekDay weekDay)
        {
            if (ModelState.IsValid)
            {
                db.Entry(weekDay).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(weekDay);
        }

        // GET: WeekDays/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WeekDay weekDay = db.WeekDays.Find(id);
            if (weekDay == null)
            {
                return HttpNotFound();
            }
            return View(weekDay);
        }

        // POST: WeekDays/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            WeekDay weekDay = db.WeekDays.Find(id);
            db.WeekDays.Remove(weekDay);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
