﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class ServiceTypeWorkingDaysController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();

        // GET: ServiceTypeWorkingDays
        public ActionResult Index()
        {
            var serviceTypeWorkingDays = db.ServiceTypeWorkingDays.Include(s => s.Organisation).Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay);
            return View(serviceTypeWorkingDays.ToList());
        }

        // GET: ServiceTypeWorkingDays/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceTypeWorkingDay serviceTypeWorkingDay = db.ServiceTypeWorkingDays.Find(id);
            if (serviceTypeWorkingDay == null)
            {
                return HttpNotFound();
            }
            return View(serviceTypeWorkingDay);
        }

        // GET: ServiceTypeWorkingDays/Create
        public ActionResult Create()
        {
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name");
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName");
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType");
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days");

            return View();
        }

        // POST: ServiceTypeWorkingDays/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DayId,ServiceId,ServiceTypeId,OrganisationId,WorkingDay,StartTime,EndTime")] ServiceTypeWorkingDay serviceTypeWorkingDay)
        {
            if (ModelState.IsValid)
            {
                db.ServiceTypeWorkingDays.Add(serviceTypeWorkingDay);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", serviceTypeWorkingDay.OrganisationId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName", serviceTypeWorkingDay.ServiceId);
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType", serviceTypeWorkingDay.ServiceTypeId);
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days", serviceTypeWorkingDay.DayId);
            return View(serviceTypeWorkingDay);
        }

        // GET: ServiceTypeWorkingDays/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceTypeWorkingDay serviceTypeWorkingDay = db.ServiceTypeWorkingDays.Find(id);
            if (serviceTypeWorkingDay == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", serviceTypeWorkingDay.OrganisationId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName", serviceTypeWorkingDay.ServiceId);
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType", serviceTypeWorkingDay.ServiceTypeId);
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days", serviceTypeWorkingDay.DayId);
            return View(serviceTypeWorkingDay);
        }

        // POST: ServiceTypeWorkingDays/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DayId,ServiceId,ServiceTypeId,OrganisationId,WorkingDay,StartTime,EndTime")] ServiceTypeWorkingDay serviceTypeWorkingDay)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceTypeWorkingDay).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", serviceTypeWorkingDay.OrganisationId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName", serviceTypeWorkingDay.ServiceId);
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType", serviceTypeWorkingDay.ServiceTypeId);
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days", serviceTypeWorkingDay.DayId);
            return View(serviceTypeWorkingDay);
        }

        // GET: ServiceTypeWorkingDays/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceTypeWorkingDay serviceTypeWorkingDay = db.ServiceTypeWorkingDays.Find(id);
            if (serviceTypeWorkingDay == null)
            {
                return HttpNotFound();
            }
            return View(serviceTypeWorkingDay);
        }

        // POST: ServiceTypeWorkingDays/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServiceTypeWorkingDay serviceTypeWorkingDay = db.ServiceTypeWorkingDays.Find(id);
            db.ServiceTypeWorkingDays.Remove(serviceTypeWorkingDay);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
