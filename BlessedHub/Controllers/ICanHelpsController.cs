﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class ICanHelpsController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();

        // GET: ICanHelps
        public ActionResult Index()
        {
            var iCanHelps = db.ICanHelps.Include(i => i.GiveHelpToOrganisaiton);
            return View(iCanHelps.ToList());
        }

        // GET: ICanHelps/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ICanHelp iCanHelp = db.ICanHelps.Find(id);
            if (iCanHelp == null)
            {
                return HttpNotFound();
            }
            return View(iCanHelp);
        }

        // GET: ICanHelps/Create
        public ActionResult Create()
        {
            ViewBag.GiveHelpToOrganisaitonId = new SelectList(db.GiveHelpToOrganisaitons, "Id", "Description");
            return View();
        }

        // POST: ICanHelps/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GiveHelpToOrganisaitonId,Email,Message,Descrption")] ICanHelp iCanHelp)
        {
            if (ModelState.IsValid)
            {
                db.ICanHelps.Add(iCanHelp);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GiveHelpToOrganisaitonId = new SelectList(db.GiveHelpToOrganisaitons, "Id", "Description", iCanHelp.GiveHelpToOrganisaitonId);
            return View(iCanHelp);
        }

        // GET: ICanHelps/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ICanHelp iCanHelp = db.ICanHelps.Find(id);
            if (iCanHelp == null)
            {
                return HttpNotFound();
            }
            ViewBag.GiveHelpToOrganisaitonId = new SelectList(db.GiveHelpToOrganisaitons, "Id", "Description", iCanHelp.GiveHelpToOrganisaitonId);
            return View(iCanHelp);
        }

        // POST: ICanHelps/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GiveHelpToOrganisaitonId,Email,Message,Descrption")] ICanHelp iCanHelp)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iCanHelp).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GiveHelpToOrganisaitonId = new SelectList(db.GiveHelpToOrganisaitons, "Id", "Description", iCanHelp.GiveHelpToOrganisaitonId);
            return View(iCanHelp);
        }

        // GET: ICanHelps/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ICanHelp iCanHelp = db.ICanHelps.Find(id);
            if (iCanHelp == null)
            {
                return HttpNotFound();
            }
            return View(iCanHelp);
        }

        // POST: ICanHelps/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ICanHelp iCanHelp = db.ICanHelps.Find(id);
            db.ICanHelps.Remove(iCanHelp);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
