﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class TimeTableServicesController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();

        // GET: TimeTableServices
        public ActionResult Index()
        {
            var timeTableServices = db.TimeTableServices.Include(t => t.ServicesTiming);
            return View(timeTableServices.ToList());
        }

        // GET: TimeTableServices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeTableService timeTableService = db.TimeTableServices.Find(id);
            if (timeTableService == null)
            {
                return HttpNotFound();
            }
            return View(timeTableService);
        }

        // GET: TimeTableServices/Create
        public ActionResult Create()
        {
            ViewBag.ServiceTimingId = new SelectList(db.ServicesTimings, "ServiceTimingId", "ServiceTimingId");
            return View();
        }

        // POST: TimeTableServices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TimeTableServicesId,StartTime,EndTime,ServiceTimingId")] TimeTableService timeTableService)
        {
            if (ModelState.IsValid)
            {
                db.TimeTableServices.Add(timeTableService);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ServiceTimingId = new SelectList(db.ServicesTimings, "ServiceTimingId", "ServiceTimingId", timeTableService.ServiceTimingId);
            return View(timeTableService);
        }

        // GET: TimeTableServices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeTableService timeTableService = db.TimeTableServices.Find(id);
            if (timeTableService == null)
            {
                return HttpNotFound();
            }
            ViewBag.ServiceTimingId = new SelectList(db.ServicesTimings, "ServiceTimingId", "ServiceTimingId", timeTableService.ServiceTimingId);
            return View(timeTableService);
        }

        // POST: TimeTableServices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "TimeTableServicesId,StartTime,EndTime,ServiceTimingId")] TimeTableService timeTableService)
        {
            if (ModelState.IsValid)
            {
                db.Entry(timeTableService).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ServiceTimingId = new SelectList(db.ServicesTimings, "ServiceTimingId", "ServiceTimingId", timeTableService.ServiceTimingId);
            return View(timeTableService);
        }

        // GET: TimeTableServices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeTableService timeTableService = db.TimeTableServices.Find(id);
            if (timeTableService == null)
            {
                return HttpNotFound();
            }
            return View(timeTableService);
        }

        // POST: TimeTableServices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TimeTableService timeTableService = db.TimeTableServices.Find(id);
            db.TimeTableServices.Remove(timeTableService);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
