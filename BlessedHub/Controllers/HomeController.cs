﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class HomeController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PartialCityListSearch()
        {
            ViewBag.CityId = new SelectList(db.Cities, "CityId", "CityName");

            return PartialView("_PartialCityListSearch");
        }

        public ActionResult PartialServicesListSearch()
        {
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName");

            return PartialView("_PartialServicesListSearch");
        }


        public ActionResult Search(OrganisationService os)
        {
            string postcode = os.Organisation.PostCode;
            int serviceid = os.ServiceId;

            //RedirectToAction("Details", "TestEF", new { id = 1 });

            //if (!ModelState.IsValid)
            //{

            //    return null;

            //}

            return RedirectToAction("DetailsCitySearch", "TestEF",new { id = serviceid , postcode });
        }





    }
}