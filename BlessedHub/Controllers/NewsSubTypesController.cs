﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class NewsSubTypesController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();

        // GET: NewsSubTypes
        public ActionResult Index()
        {
            var newsSubTypes = db.NewsSubTypes.Include(n => n.NewsType);
            return View(newsSubTypes.ToList());
        }

        // GET: NewsSubTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsSubType newsSubType = db.NewsSubTypes.Find(id);
            if (newsSubType == null)
            {
                return HttpNotFound();
            }
            return View(newsSubType);
        }

        // GET: NewsSubTypes/Create
        public ActionResult Create()
        {
            ViewBag.NewsTypeId = new SelectList(db.NewsTypes, "Id", "NewsType1");
            return View();
        }

        // POST: NewsSubTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NewsTypeId,NewsSubType1,Description")] NewsSubType newsSubType)
        {
            if (ModelState.IsValid)
            {
                db.NewsSubTypes.Add(newsSubType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.NewsTypeId = new SelectList(db.NewsTypes, "Id", "NewsType1", newsSubType.NewsTypeId);
            return View(newsSubType);
        }

        // GET: NewsSubTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsSubType newsSubType = db.NewsSubTypes.Find(id);
            if (newsSubType == null)
            {
                return HttpNotFound();
            }
            ViewBag.NewsTypeId = new SelectList(db.NewsTypes, "Id", "NewsType1", newsSubType.NewsTypeId);
            return View(newsSubType);
        }

        // POST: NewsSubTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NewsTypeId,NewsSubType1,Description")] NewsSubType newsSubType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(newsSubType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.NewsTypeId = new SelectList(db.NewsTypes, "Id", "NewsType1", newsSubType.NewsTypeId);
            return View(newsSubType);
        }

        // GET: NewsSubTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            NewsSubType newsSubType = db.NewsSubTypes.Find(id);
            if (newsSubType == null)
            {
                return HttpNotFound();
            }
            return View(newsSubType);
        }

        // POST: NewsSubTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            NewsSubType newsSubType = db.NewsSubTypes.Find(id);
            db.NewsSubTypes.Remove(newsSubType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
