﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class GiveHelpTypesController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();

        // GET: GiveHelpTypes
        public ActionResult Index()
        {
            return View(db.GiveHelpTypes.ToList());
        }

        // GET: GiveHelpTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiveHelpType giveHelpType = db.GiveHelpTypes.Find(id);
            if (giveHelpType == null)
            {
                return HttpNotFound();
            }
            return View(giveHelpType);
        }

        // GET: GiveHelpTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GiveHelpTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,HelpType,Description")] GiveHelpType giveHelpType)
        {
            if (ModelState.IsValid)
            {
                db.GiveHelpTypes.Add(giveHelpType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(giveHelpType);
        }

        // GET: GiveHelpTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiveHelpType giveHelpType = db.GiveHelpTypes.Find(id);
            if (giveHelpType == null)
            {
                return HttpNotFound();
            }
            return View(giveHelpType);
        }

        // POST: GiveHelpTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,HelpType,Description")] GiveHelpType giveHelpType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(giveHelpType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(giveHelpType);
        }

        // GET: GiveHelpTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiveHelpType giveHelpType = db.GiveHelpTypes.Find(id);
            if (giveHelpType == null)
            {
                return HttpNotFound();
            }
            return View(giveHelpType);
        }

        // POST: GiveHelpTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GiveHelpType giveHelpType = db.GiveHelpTypes.Find(id);
            db.GiveHelpTypes.Remove(giveHelpType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
