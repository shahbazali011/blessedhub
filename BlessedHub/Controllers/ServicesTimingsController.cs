﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class ServicesTimingsController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();

        // GET: ServicesTimings
        public ActionResult Index()
        {
            var servicesTimings = db.ServicesTimings.Include(s => s.Organisation).Include(s => s.ServicesType);
            return View(servicesTimings.ToList());
        }

        // GET: ServicesTimings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServicesTiming servicesTiming = db.ServicesTimings.Find(id);
            if (servicesTiming == null)
            {
                return HttpNotFound();
            }
            return View(servicesTiming);
        }

        // GET: ServicesTimings/Create
        public ActionResult Create()
        {
            ViewBag.OrganisaitonId = new SelectList(db.Organisations, "OrganisationId", "Name");
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType");
            return View();
        }

        // POST: ServicesTimings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ServiceTimingId,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,OrganisaitonId,ServiceTypeId")] ServicesTiming servicesTiming)
        {
            if (ModelState.IsValid)
            {
                db.ServicesTimings.Add(servicesTiming);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OrganisaitonId = new SelectList(db.Organisations, "OrganisationId", "Name", servicesTiming.OrganisaitonId);
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType", servicesTiming.ServiceTypeId);
            return View(servicesTiming);
        }

        // GET: ServicesTimings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServicesTiming servicesTiming = db.ServicesTimings.Find(id);
            if (servicesTiming == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganisaitonId = new SelectList(db.Organisations, "OrganisationId", "Name", servicesTiming.OrganisaitonId);
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType", servicesTiming.ServiceTypeId);
            return View(servicesTiming);
        }

        // POST: ServicesTimings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ServiceTimingId,Monday,Tuesday,Wednesday,Thursday,Friday,Saturday,Sunday,OrganisaitonId,ServiceTypeId")] ServicesTiming servicesTiming)
        {
            if (ModelState.IsValid)
            {
                db.Entry(servicesTiming).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrganisaitonId = new SelectList(db.Organisations, "OrganisationId", "Name", servicesTiming.OrganisaitonId);
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType", servicesTiming.ServiceTypeId);
            return View(servicesTiming);
        }

        // GET: ServicesTimings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServicesTiming servicesTiming = db.ServicesTimings.Find(id);
            if (servicesTiming == null)
            {
                return HttpNotFound();
            }
            return View(servicesTiming);
        }

        // POST: ServicesTimings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServicesTiming servicesTiming = db.ServicesTimings.Find(id);
            db.ServicesTimings.Remove(servicesTiming);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
