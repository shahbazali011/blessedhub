﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class GiveHelpSubTypesController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();

        // GET: GiveHelpSubTypes
        public ActionResult Index()
        {
            var giveHelpSubTypes = db.GiveHelpSubTypes.Include(g => g.GiveHelpType);
            return View(giveHelpSubTypes.ToList());
        }

        // GET: GiveHelpSubTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiveHelpSubType giveHelpSubType = db.GiveHelpSubTypes.Find(id);
            if (giveHelpSubType == null)
            {
                return HttpNotFound();
            }
            return View(giveHelpSubType);
        }

        // GET: GiveHelpSubTypes/Create
        public ActionResult Create()
        {
            ViewBag.GiveHelpTypesId = new SelectList(db.GiveHelpTypes, "Id", "HelpType");
            return View();
        }

        // POST: GiveHelpSubTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GiveHelpTypesId,HelpSubType,Description")] GiveHelpSubType giveHelpSubType)
        {
            if (ModelState.IsValid)
            {
                db.GiveHelpSubTypes.Add(giveHelpSubType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GiveHelpTypesId = new SelectList(db.GiveHelpTypes, "Id", "HelpType", giveHelpSubType.GiveHelpTypesId);
            return View(giveHelpSubType);
        }

        // GET: GiveHelpSubTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiveHelpSubType giveHelpSubType = db.GiveHelpSubTypes.Find(id);
            if (giveHelpSubType == null)
            {
                return HttpNotFound();
            }
            ViewBag.GiveHelpTypesId = new SelectList(db.GiveHelpTypes, "Id", "HelpType", giveHelpSubType.GiveHelpTypesId);
            return View(giveHelpSubType);
        }

        // POST: GiveHelpSubTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GiveHelpTypesId,HelpSubType,Description")] GiveHelpSubType giveHelpSubType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(giveHelpSubType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GiveHelpTypesId = new SelectList(db.GiveHelpTypes, "Id", "HelpType", giveHelpSubType.GiveHelpTypesId);
            return View(giveHelpSubType);
        }

        // GET: GiveHelpSubTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiveHelpSubType giveHelpSubType = db.GiveHelpSubTypes.Find(id);
            if (giveHelpSubType == null)
            {
                return HttpNotFound();
            }
            return View(giveHelpSubType);
        }

        // POST: GiveHelpSubTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GiveHelpSubType giveHelpSubType = db.GiveHelpSubTypes.Find(id);
            db.GiveHelpSubTypes.Remove(giveHelpSubType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
