﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.Presistence.Repositories;

namespace BlessedHub.Controllers
{
    public class ServiceTypeWorkingDaysDataController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;
        public ServiceTypeWorkingDaysDataController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

        }

        // GET: ServiceTypeWorkingDaysData
        public ActionResult Index()
        {
            var serviceTypeWorkingDays = db.ServiceTypeWorkingDays.Include(s => s.Organisation).Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay);
            return View(serviceTypeWorkingDays.ToList());
        }

        // GET: ServiceTypeWorkingDaysData/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceTypeWorkingDay serviceTypeWorkingDay = db.ServiceTypeWorkingDays.Find(id);
            if (serviceTypeWorkingDay == null)
            {
                return HttpNotFound();
            }
            return View(serviceTypeWorkingDay);
        }

        public ActionResult PartialAllServicesTypesDetails(int? id)
        {
            //var serviceTypeWorkingDays = db.ServiceTypeWorkingDays.Where(s =>s.ServiceId==id).Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay);
            //return PartialView("_PartialAllServicesTypesDetails", serviceTypeWorkingDays.ToList());
            return PartialView("_PartialAllServicesTypesDetails", _unitOfWork.ServiceTypeWorkingDaysData.PartialAllServicesTypesDetails(id));

        }

        public ActionResult PartialMapOrganisation(int? id)
        {
            //var serviceTypeWorkingDays = db.ServiceTypeWorkingDays.Where(s => s.ServiceId == id).Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay);

            var serviceTypeWorkingDays = _unitOfWork.ServiceTypeWorkingDaysData.PartialMapOrganisation(id);

            string markers = "[";
            foreach (var sdr in serviceTypeWorkingDays)
            {
                markers += "{";
                markers += string.Format("'title': '{0}',", sdr.OrganisationId);
                markers += string.Format("'description': '{0}',", sdr.Organisation.Description);
                markers += string.Format("'lat': '{0}',", sdr.Organisation.Latitude);
                markers += string.Format("'lng': '{0}'", sdr.Organisation.Longitude);
                markers += "},";
            }
            markers += "];";

            ViewBag.Markers = markers;

            return PartialView("_PartialMapOrganisation");
        }



        public ActionResult PartialMapOrganisationPostCode(string postcode)
        {

            var PostCodeList = _unitOfWork.Organisations.GetPostCodeList(postcode);


            string markers = "[";
            foreach (var sdr in PostCodeList)
            {
                markers += "{";
                markers += string.Format("'title': '{0}',", sdr.OrganisationId);
                markers += string.Format("'description': '{0}',", sdr.Description);
                markers += string.Format("'lat': '{0}',", sdr.Latitude);
                markers += string.Format("'lng': '{0}'", sdr.Longitude);
                markers += "},";
            }
            markers += "];";

            ViewBag.Markers = markers;

            return PartialView("_PartialMapOrganisation");
        }





        public ActionResult PartialServiceTypeTimeTable(int? ServiceTypeId,int? ServiceId, int? OrganisationId)
        {
            if (ServiceTypeId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var serviceTypeWorkingDays = db.ServiceTypeWorkingDays
                .Where(st =>st.ServiceTypeId== ServiceTypeId && st.ServiceId==1 && st.OrganisationId==1)
                .Include(s => s.Service).Include(s => s.ServicesType).Include(s => s.WeekDay).Include(s => s.Organisation);
            if (serviceTypeWorkingDays == null)
            {
                return HttpNotFound();
            }
            return PartialView("_PartialServiceTypeTimeTable", serviceTypeWorkingDays.ToList());
        }

        // GET: ServiceTypeWorkingDays/Create
        public ActionResult Create()
        {
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name");
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName");
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType");
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days");

            return View();
        }

        // POST: ServiceTypeWorkingDays/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DayId,ServiceId,ServiceTypeId,OrganisationId,WorkingDay,StartTime,EndTime")] ServiceTypeWorkingDay serviceTypeWorkingDay)
        {
            if (ModelState.IsValid)
            {
                db.ServiceTypeWorkingDays.Add(serviceTypeWorkingDay);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", serviceTypeWorkingDay.OrganisationId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName", serviceTypeWorkingDay.ServiceId);
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType", serviceTypeWorkingDay.ServiceTypeId);
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days", serviceTypeWorkingDay.DayId);
            return View(serviceTypeWorkingDay);
        }

        // GET: ServiceTypeWorkingDays/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceTypeWorkingDay serviceTypeWorkingDay = db.ServiceTypeWorkingDays.Find(id);
            if (serviceTypeWorkingDay == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", serviceTypeWorkingDay.OrganisationId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName", serviceTypeWorkingDay.ServiceId);
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType", serviceTypeWorkingDay.ServiceTypeId);
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days", serviceTypeWorkingDay.DayId);
            return View(serviceTypeWorkingDay);
        }

        // POST: ServiceTypeWorkingDays/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DayId,ServiceId,ServiceTypeId,OrganisationId,WorkingDay,StartTime,EndTime")] ServiceTypeWorkingDay serviceTypeWorkingDay)
        {
            if (ModelState.IsValid)
            {
                db.Entry(serviceTypeWorkingDay).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", serviceTypeWorkingDay.OrganisationId);
            ViewBag.ServiceId = new SelectList(db.Services, "ServiceId", "ServiceName", serviceTypeWorkingDay.ServiceId);
            ViewBag.ServiceTypeId = new SelectList(db.ServicesTypes, "ServiceTypeId", "ServiceType", serviceTypeWorkingDay.ServiceTypeId);
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days", serviceTypeWorkingDay.DayId);
            return View(serviceTypeWorkingDay);
        }

        // GET: ServiceTypeWorkingDays/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ServiceTypeWorkingDay serviceTypeWorkingDay = db.ServiceTypeWorkingDays.Find(id);
            if (serviceTypeWorkingDay == null)
            {
                return HttpNotFound();
            }
            return View(serviceTypeWorkingDay);
        }

        // POST: ServiceTypeWorkingDays/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ServiceTypeWorkingDay serviceTypeWorkingDay = db.ServiceTypeWorkingDays.Find(id);
            db.ServiceTypeWorkingDays.Remove(serviceTypeWorkingDay);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
