﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.Presistence.Repositories;

namespace BlessedHub.Controllers
{
    public class OrganisationWorkingDaysController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();
        private readonly IUnitOfWork _unitOfWork;
        public OrganisationWorkingDaysController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;

        }


        // GET: OrganisationWorkingDays
        public ActionResult Index()
        {
            var organisationWorkingDays = db.OrganisationWorkingDays.Include(o => o.Organisation).Include(o => o.WeekDay);
            return View(organisationWorkingDays.ToList());
        }

        // GET: OrganisationWorkingDays/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganisationWorkingDay organisationWorkingDay = db.OrganisationWorkingDays.Find(id);
            if (organisationWorkingDay == null)
            {
                return HttpNotFound();
            }
            return View(organisationWorkingDay);
        }

        public ActionResult OranisationTimeTable(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //var organisationWorkingDays = db.OrganisationWorkingDays.Where(o => o.OrganisationId == id).Include(o => o.Organisation).Include(o => o.WeekDay);
            var organisationWorkingDays = _unitOfWork.OrganisationWorkingDays.OranisationTimeTable(id);

            if (organisationWorkingDays == null)
            {
                return HttpNotFound();
            }
            return PartialView("_PartialOrganisaitonTimeTable", organisationWorkingDays);

        }

        // GET: OrganisationWorkingDays/Create
        public ActionResult Create()
        {
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name");
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days");
            return View();
        }

        // POST: OrganisationWorkingDays/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DayId,OrganisationId,WorkingDay,StartTime,EndTime")] OrganisationWorkingDay organisationWorkingDay)
        {
            if (ModelState.IsValid)
            {
                db.OrganisationWorkingDays.Add(organisationWorkingDay);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", organisationWorkingDay.OrganisationId);
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days", organisationWorkingDay.DayId);
            return View(organisationWorkingDay);
        }

        // GET: OrganisationWorkingDays/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganisationWorkingDay organisationWorkingDay = db.OrganisationWorkingDays.Find(id);
            if (organisationWorkingDay == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", organisationWorkingDay.OrganisationId);
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days", organisationWorkingDay.DayId);
            return View(organisationWorkingDay);
        }

        // POST: OrganisationWorkingDays/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DayId,OrganisationId,WorkingDay,StartTime,EndTime")] OrganisationWorkingDay organisationWorkingDay)
        {
            if (ModelState.IsValid)
            {
                db.Entry(organisationWorkingDay).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", organisationWorkingDay.OrganisationId);
            ViewBag.DayId = new SelectList(db.WeekDays, "Id", "Days", organisationWorkingDay.DayId);
            return View(organisationWorkingDay);
        }

        // GET: OrganisationWorkingDays/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OrganisationWorkingDay organisationWorkingDay = db.OrganisationWorkingDays.Find(id);
            if (organisationWorkingDay == null)
            {
                return HttpNotFound();
            }
            return View(organisationWorkingDay);
        }

        // POST: OrganisationWorkingDays/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OrganisationWorkingDay organisationWorkingDay = db.OrganisationWorkingDays.Find(id);
            db.OrganisationWorkingDays.Remove(organisationWorkingDay);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
