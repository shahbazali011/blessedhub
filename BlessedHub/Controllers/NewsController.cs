﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;
using BlessedHub.ViewModels;
using AutoMapper;

namespace BlessedHub.Controllers
{
    public class NewsController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();

        // GET: News
        public ActionResult Index()
        {
            var news = db.News.Include(n => n.NewsSubType).Include(n => n.NewsType);
            return View(news.ToList());
        }
        public ActionResult NewsStories()
        {
            var news = db.News.Include(n => n.NewsSubType).Include(n => n.NewsType);
            return View("NewsStories", news.ToList());
        }
        // GET: News/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNews(NewsVM vmModel)
        {

            string filePath = string.Empty;
            string fileContentType = string.Empty;
          

            try
            {
                // Verification
                if (ModelState.IsValid)
                {
                    // Converting to bytes.
                    byte[] uploadedFile = new byte[vmModel.FileAttach.InputStream.Length];
                    vmModel.FileAttach.InputStream.Read(uploadedFile, 0, uploadedFile.Length);

                    // Initialization.
                    fileContentType = vmModel.FileAttach.ContentType;
                    string folderPath = "/Images/News/";
                    this.WriteBytesToFile(this.Server.MapPath(folderPath), uploadedFile, vmModel.FileAttach.FileName);
                    filePath = folderPath + vmModel.FileAttach.FileName;

                    // Saving info.
                }

                var test = new News();

                //Mapper.Map(vmModel,test);


                var news = new News
                {
                    NewsTitle = vmModel.NewsTitle,
                    NewsDetail = vmModel.NewsDetail,
                    MoreDetails = vmModel.MoreDetails,
                    PostedDate = vmModel.PostedDate,
                    NewsTypeId = vmModel.NewsTypeId,
                    NewsSubTypeId = vmModel.NewsSubTypeId,
                    FacebookLink = vmModel.FacebookLink,
                    TwitterLink = vmModel.TwitterLink,
                    Tagged = vmModel.Tagged,
                    PictureURL = filePath
                };

                if (ModelState.IsValid)
                {
                    db.News.Add(news);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                // Info
                Console.Write(ex);
            }

            return View();
        }

             private void WriteBytesToFile(string rootFolderPath, byte[] fileBytes, string filename)
            {
                try
                {
                    // Verification.
                    if (!Directory.Exists(rootFolderPath))
                    {
                        // Initialization.
                        string fullFolderPath = rootFolderPath;

                        // Settings.
                        string folderPath = new Uri(fullFolderPath).LocalPath;

                        // Create.
                        Directory.CreateDirectory(folderPath);
                    }

                    // Initialization.                
                    string fullFilePath = rootFolderPath + filename;

                    // Create.
                    FileStream fs = System.IO.File.Create(fullFilePath);

                    // Close.
                    fs.Flush();
                    fs.Dispose();
                    fs.Close();

                    // Write Stream.
                    BinaryWriter sw = new BinaryWriter(new FileStream(fullFilePath, FileMode.Create, FileAccess.Write));

                    // Write to file.
                    sw.Write(fileBytes);

                    // Closing.
                    sw.Flush();
                    sw.Dispose();
                    sw.Close();
                }
                catch (Exception ex)
                {
                    // Info.
                    throw ex;
                }
            }


            // GET: News/Create
            public ActionResult Create()
        {
            ViewBag.NewsSubTypeId = new SelectList(db.NewsSubTypes, "Id", "NewsSubType1");
            ViewBag.NewsTypeId = new SelectList(db.NewsTypes, "Id", "NewsType1");
            return View();
        }

        // POST: News/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NewsTitle,NewsDetail,MoreDetails,PostedDate,PostedBy,NewsTypeId,NewsSubTypeId,FacebookLink,TwitterLink,Tagged,PictureURL")] News news)
        {
            if (ModelState.IsValid)
            {
                db.News.Add(news);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.NewsSubTypeId = new SelectList(db.NewsSubTypes, "Id", "NewsSubType1", news.NewsSubTypeId);
            ViewBag.NewsTypeId = new SelectList(db.NewsTypes, "Id", "NewsType1", news.NewsTypeId);
            return View(news);
        }

        // GET: News/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            ViewBag.NewsSubTypeId = new SelectList(db.NewsSubTypes, "Id", "NewsSubType1", news.NewsSubTypeId);
            ViewBag.NewsTypeId = new SelectList(db.NewsTypes, "Id", "NewsType1", news.NewsTypeId);
            return View(news);
        }

        // POST: News/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NewsTitle,NewsDetail,MoreDetails,PostedDate,PostedBy,NewsTypeId,NewsSubTypeId,FacebookLink,TwitterLink,Tagged,PictureURL")] News news)
        {
            if (ModelState.IsValid)
            {
                db.Entry(news).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.NewsSubTypeId = new SelectList(db.NewsSubTypes, "Id", "NewsSubType1", news.NewsSubTypeId);
            ViewBag.NewsTypeId = new SelectList(db.NewsTypes, "Id", "NewsType1", news.NewsTypeId);
            return View(news);
        }

        // GET: News/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // POST: News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            News news = db.News.Find(id);
            db.News.Remove(news);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
