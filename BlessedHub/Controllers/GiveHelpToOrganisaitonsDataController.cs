﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BlessedHub.Models;

namespace BlessedHub.Controllers
{
    public class GiveHelpToOrganisaitonsDataController : Controller
    {
        private BlessedHubEntities db = new BlessedHubEntities();

        // GET: GiveHelpToOrganisaitonsData
        public ActionResult Index()
        {
            var giveHelpToOrganisaitons = db.GiveHelpToOrganisaitons.Include(g => g.GiveHelpSubType).Include(g => g.Organisation).Include(g => g.GiveHelpType);
            return View(giveHelpToOrganisaitons.ToList());
        }


        public ActionResult Help()
        {
            var giveHelpToOrganisaitons = db.GiveHelpToOrganisaitons.Include(g => g.GiveHelpSubType).Include(g => g.Organisation).Include(g => g.GiveHelpType);
            return View(giveHelpToOrganisaitons.ToList());
        }

        public ActionResult PartialHelpItems()
        {
            var giveHelpToOrganisaitons = db.GiveHelpToOrganisaitons.Where(ht =>ht.GiveHelpType.Id==1).Include(g => g.GiveHelpSubType).Include(g => g.Organisation).Include(g => g.GiveHelpType);

            return PartialView("_PartialHelpItems", giveHelpToOrganisaitons.ToList());
        }
        public ActionResult PartialHelpTime()
        {
            var giveHelpToOrganisaitons = db.GiveHelpToOrganisaitons.Where(ht => ht.GiveHelpType.Id == 3).Include(g => g.GiveHelpSubType).Include(g => g.Organisation).Include(g => g.GiveHelpType);

            return PartialView("_PartialHelpTime", giveHelpToOrganisaitons.ToList());
        }
        public ActionResult PartialHelpMoney()
        {
            var giveHelpToOrganisaitons = db.GiveHelpToOrganisaitons.Where(ht => ht.GiveHelpType.Id == 2).Include(g => g.GiveHelpSubType).Include(g => g.Organisation).Include(g => g.GiveHelpType);

            return PartialView("_PartialHelpMoney", giveHelpToOrganisaitons.ToList());
        }


        // GET: GiveHelpToOrganisaitonsData/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiveHelpToOrganisaiton giveHelpToOrganisaiton = db.GiveHelpToOrganisaitons.Find(id);
            if (giveHelpToOrganisaiton == null)
            {
                return HttpNotFound();
            }
            return View(giveHelpToOrganisaiton);
        }

        // GET: GiveHelpToOrganisaitonsData/Create
        public ActionResult Create()
        {
            ViewBag.GiveHelpSubTypesId = new SelectList(db.GiveHelpSubTypes, "Id", "HelpSubType");
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name");
            ViewBag.GiveHelpTypesId = new SelectList(db.GiveHelpTypes, "Id", "HelpType");
            return View();
        }

        // POST: GiveHelpToOrganisaitonsData/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,GiveHelpSubTypesId,OrganisationId,Description,GiveHelpTypesId")] GiveHelpToOrganisaiton giveHelpToOrganisaiton)
        {
            if (ModelState.IsValid)
            {
                db.GiveHelpToOrganisaitons.Add(giveHelpToOrganisaiton);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.GiveHelpSubTypesId = new SelectList(db.GiveHelpSubTypes, "Id", "HelpSubType", giveHelpToOrganisaiton.GiveHelpSubTypesId);
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", giveHelpToOrganisaiton.OrganisationId);
            ViewBag.GiveHelpTypesId = new SelectList(db.GiveHelpTypes, "Id", "HelpType", giveHelpToOrganisaiton.GiveHelpTypesId);
            return View(giveHelpToOrganisaiton);
        }

        // GET: GiveHelpToOrganisaitonsData/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiveHelpToOrganisaiton giveHelpToOrganisaiton = db.GiveHelpToOrganisaitons.Find(id);
            if (giveHelpToOrganisaiton == null)
            {
                return HttpNotFound();
            }
            ViewBag.GiveHelpSubTypesId = new SelectList(db.GiveHelpSubTypes, "Id", "HelpSubType", giveHelpToOrganisaiton.GiveHelpSubTypesId);
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", giveHelpToOrganisaiton.OrganisationId);
            ViewBag.GiveHelpTypesId = new SelectList(db.GiveHelpTypes, "Id", "HelpType", giveHelpToOrganisaiton.GiveHelpTypesId);
            return View(giveHelpToOrganisaiton);
        }

        // POST: GiveHelpToOrganisaitonsData/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,GiveHelpSubTypesId,OrganisationId,Description,GiveHelpTypesId")] GiveHelpToOrganisaiton giveHelpToOrganisaiton)
        {
            if (ModelState.IsValid)
            {
                db.Entry(giveHelpToOrganisaiton).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.GiveHelpSubTypesId = new SelectList(db.GiveHelpSubTypes, "Id", "HelpSubType", giveHelpToOrganisaiton.GiveHelpSubTypesId);
            ViewBag.OrganisationId = new SelectList(db.Organisations, "OrganisationId", "Name", giveHelpToOrganisaiton.OrganisationId);
            ViewBag.GiveHelpTypesId = new SelectList(db.GiveHelpTypes, "Id", "HelpType", giveHelpToOrganisaiton.GiveHelpTypesId);
            return View(giveHelpToOrganisaiton);
        }

        // GET: GiveHelpToOrganisaitonsData/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            GiveHelpToOrganisaiton giveHelpToOrganisaiton = db.GiveHelpToOrganisaitons.Find(id);
            if (giveHelpToOrganisaiton == null)
            {
                return HttpNotFound();
            }
            return View(giveHelpToOrganisaiton);
        }

        // POST: GiveHelpToOrganisaitonsData/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            GiveHelpToOrganisaiton giveHelpToOrganisaiton = db.GiveHelpToOrganisaitons.Find(id);
            db.GiveHelpToOrganisaitons.Remove(giveHelpToOrganisaiton);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
