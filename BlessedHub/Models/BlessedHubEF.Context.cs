﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BlessedHub.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class BlessedHubEntities : DbContext
    {
        public BlessedHubEntities()
            : base("name=BlessedHubEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<City> Cities { get; set; }
        public virtual DbSet<OrganisaitonServicesType> OrganisaitonServicesTypes { get; set; }
        public virtual DbSet<Organisation> Organisations { get; set; }
        public virtual DbSet<OrganisationService> OrganisationServices { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<ServicesType> ServicesTypes { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<OrganisationWorkingDay> OrganisationWorkingDays { get; set; }
        public virtual DbSet<WeekDay> WeekDays { get; set; }
        public virtual DbSet<GiveHelpSubType> GiveHelpSubTypes { get; set; }
        public virtual DbSet<GiveHelpToOrganisaiton> GiveHelpToOrganisaitons { get; set; }
        public virtual DbSet<GiveHelpType> GiveHelpTypes { get; set; }
        public virtual DbSet<ServiceTypeWorkingDay> ServiceTypeWorkingDays { get; set; }
        public virtual DbSet<ICanHelp> ICanHelps { get; set; }
        public virtual DbSet<Volunteer> Volunteers { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<NewsSubType> NewsSubTypes { get; set; }
        public virtual DbSet<NewsType> NewsTypes { get; set; }
    }
}
