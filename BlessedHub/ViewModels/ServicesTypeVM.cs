﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class ServicesTypeVM
    {
        public int ServiceTypeId { get; set; }
        public string ServiceType { get; set; }
        public int ServiceId { get; set; }
        public string Description { get; set; }
        public Nullable<bool> Active { get; set; }

    }
}