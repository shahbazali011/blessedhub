﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class ServiceTypeWorkingDayVM
    {
        public int Id { get; set; }
        public int DayId { get; set; }
        public int ServiceId { get; set; }
        public int ServiceTypeId { get; set; }
        public int OrganisationId { get; set; }
        public Nullable<bool> WorkingDay { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}