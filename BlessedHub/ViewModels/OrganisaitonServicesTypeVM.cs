﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class OrganisaitonServicesTypeVM
    {
        public int OrganisaitonServicesTypesId { get; set; }
        public int ServiceTypeId { get; set; }
        public int OrganisationId { get; set; }
        public string SuitableFor { get; set; }
        public string Description { get; set; }
        public int ServiceId { get; set; }
    }
}