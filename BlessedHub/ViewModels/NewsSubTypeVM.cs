﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class NewsSubTypeVM
    {
        public int Id { get; set; }
        public int NewsTypeId { get; set; }
        public string NewsSubType1 { get; set; }
        public string Description { get; set; }
    }
}