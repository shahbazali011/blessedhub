﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class VolunteerVM
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string TelephoneNumber { get; set; }
        public int CityId { get; set; }
        public string PostCode { get; set; }
        public string Skills { get; set; }
        public string Availablity { get; set; }
        public string Resources { get; set; }
    }
}