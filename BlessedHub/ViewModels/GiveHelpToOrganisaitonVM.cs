﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class GiveHelpToOrganisaitonVM
    {
        public int Id { get; set; }
        public int GiveHelpSubTypesId { get; set; }
        public int OrganisationId { get; set; }
        public string Description { get; set; }
        public int GiveHelpTypesId { get; set; }
    }
}