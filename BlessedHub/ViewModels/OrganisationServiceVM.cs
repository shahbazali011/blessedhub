﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class OrganisationServiceVM
    {
        public int OrganisationServicesId { get; set; }
        public int OrganisationId { get; set; }
        public string Description { get; set; }
        public int ServiceId { get; set; }
    }
}