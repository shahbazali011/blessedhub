﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class NewsTypeVM
    {
        public int Id { get; set; }
        public string NewsType1 { get; set; }
        public string Description { get; set; }

    }
}