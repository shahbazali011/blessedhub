﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class GiveHelpTypeVM
    {
        public int Id { get; set; }
        public string HelpType { get; set; }
        public string Description { get; set; }
    }
}