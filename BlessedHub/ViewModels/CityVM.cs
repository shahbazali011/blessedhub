﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace BlessedHub.ViewModels
{
    public class CityVM
    {
        public int CityId { get; set; }

        [Required]
        public string CityName { get; set; }
        
    }
}