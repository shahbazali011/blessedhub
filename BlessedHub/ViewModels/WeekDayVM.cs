﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class WeekDayVM
    {
        public int Id { get; set; }
        public string Days { get; set; }
    }
}