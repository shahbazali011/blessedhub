﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class NewsVM
    {
        public int Id { get; set; }
        public string NewsTitle { get; set; }
        public string NewsDetail { get; set; }
        public string MoreDetails { get; set; }
        public string PostedDate { get; set; }
        public string PostedBy { get; set; }
        public int NewsTypeId { get; set; }
        public int NewsSubTypeId { get; set; }
        public string FacebookLink { get; set; }
        public string TwitterLink { get; set; }
        public string Tagged { get; set; }
        //public string PictureURL { get; set; }
        [Required]
        [Display(Name = "Upload File")]
        public HttpPostedFileBase FileAttach { get; set; }
        public Nullable<bool> Active { get; set; }

    }
}