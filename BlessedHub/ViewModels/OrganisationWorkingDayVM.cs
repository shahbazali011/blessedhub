﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class OrganisationWorkingDayVM
    {
        public int Id { get; set; }
        public Nullable<int> DayId { get; set; }
        public Nullable<int> OrganisationId { get; set; }
        public Nullable<bool> WorkingDay { get; set; }
        [Required(ErrorMessage = "Value is required")]
        [Display(Name = "Opening Time")]
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}