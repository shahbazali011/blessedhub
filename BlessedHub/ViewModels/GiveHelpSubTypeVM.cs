﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BlessedHub.ViewModels
{
    public class GiveHelpSubTypeVM
    {
        public int Id { get; set; }
        public int GiveHelpTypesId { get; set; }
        public string HelpSubType { get; set; }
        public string Description { get; set; }
    }
}